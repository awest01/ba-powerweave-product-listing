var IsBindFilterData = true;
var showfilters = 'True';
var status = false;
var htmlText = "";

$(document).ready(function () {

    $(document).ajaxStart(function () {
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function () {
        $("#wait").css("display", "none");
    });
    $("#hfSelectedSorting").val($('#ddlSorting').find("option:selected").val());

    $('#dvSearchMessage').hide();

    //Google Mixit
    if (blnIsGoogleMixIt == 'True') {

        $('#hidPageNo').val("0");
        $(function () {
            $('#dvProductData').mixitup({
                targetSelector: '.mix-target',
                filterSelector: '.filter-btn',
                //sortSelector: '.sort',
                buttonEvent: 'click',
                effects: ['scale'],
                listEffects: null,
                easing: 'snap',
                layoutMode: 'grid',
                targetDisplayGrid: 'inline-block',
                targetDisplayList: 'block',
                gridClass: '',
                listClass: '',
                transitionSpeed: 1500,
                showOnLoad: 'all',
                sortOnLoad: ['data-cat', 'desc'],
                multiFilter: false,
                filterLogic: 'or',
                resizeContainer: true,
                minHeight: 0,
                failClass: 'fail',
                perspectiveDistance: '3000',
                perspectiveOrigin: '50% 50%',
                animateGridList: true,
                onMixLoad: null,
                onMixStart: null,
                onMixEnd: null
            });
        });
    }
    /////

    GetProducts();
});

setTimeout(function () { SetDisplayClass(); }, 150);

function GetProducts() {

    $("#wait").css("display", "block");
    if ($('#hidPageNo').val() == 1 || isPaginationEnabled == 'True') {
        $('#dvProductData').html('');
        htmlText = "";
    }

    var asyncprop = true;
    var sortOrder = $('#hfSelectedSorting').val();

    if (blnIsGoogleMixIt == 'True') {
        asyncprop = false;
        sortOrder = GoogleMixitSortOrder;
    }

    $('#productsRandom').hide();
    $('#dvSearchMessage').hide();

    if (IsNoRecordOnSearch == 'True') {
        SearchProductIds = 0;
    }

    var dataType = 'application/json; charset=utf-8';

    var data = {
        categoryname: $('#hidcategoryName').val(),
        subcategoryname: $('#hidsubCategoryName').val(),
        subsubcategoryname: $('#hidsubSubCategoryName').val(),
        sectionname: $('#hidsectionName').val(),
        pageno: $('#hidPageNo').val(),
        pagesize: PageSize,
        sortname: sortOrder,
        isfinitescroll: '',
        isgooglemixit: '',
        minprice: MiniumPrice,
        maxprice: MaximumPrice,
        languageid: $('#hidLanguageId').val(),
        currencyid: $('#hidCurrencyId').val(),
        currencysymbol: $('#hidCurrencySymbol').val(),
        Colors: $('#hidColorIds').val(),
        sectionids: $('#hidSectionIds').val(),
        customfilter1: $('#hidCustomFilter1').val(),
        customfilter2: $('#hidCustomFilter2').val(),
        customfilter3: $('#hidCustomFilter3').val(),
        customfilter4: $('#hidCustomFilter4').val(),
        customfilter5: $('#hidCustomFilter5').val(),
        productids: '',
        isenablefilters: '',
        searchproductids: SearchProductIds
    };
    $.ajax({
        type: 'POST',
        url: host + '/ProductListing/BindProductDataJson',
        dataType: 'json',
        contentType: dataType,
        data: JSON.stringify(data),
        cache: false,
        async: asyncprop,
        success: function (result) {
            if (result == "norecords") {
                IsBindFilterData = true;
                if ($('#hidPageNo').val() == 1) {
                    $('#SortingDataContainer').hide();
                    $('#compareBtn').hide();

                    if (IsNoRecordOnSearch == 'True') {

                        $('#dvProductData').html(searchNoRecordMessage + ' "' + strSearchKeyword + '" <br> ' + ConvertTextToHTML(searchSuggestionMessage) + ' <br>');
                        //$('#dvSearchMessage').hide();
                        $('#productsRandom').show();
                        BindRandomProducts();
                    }
                    else {
                        $('#dvProductData').append("<span>No records found.</span>");
                    }
                    // $('#dvFilterContainer').hide();
                    // IsBindFilterData = false;
                }
                status = false;
            }
            else {
                //$('#dvFilterData').html('');
				
				// if (IsBindFilterData) {
                    // if ((showfilters == 'True') || (blnIsGoogleMixIt == 'True')) {
                        // BindFilterProduct();
                        // if (blnIsGoogleMixIt == 'True') {
                            // $('.PaginationContainer ').removeClass('col-md-5');
                            // $('.PaginationContainer ').addClass('col-md-10');
                            // $('#aMobileFilter').hide();
                        // }
                    // }
                    // else {
                        // $('#dvLoader').hide();
                    // }
                // } 


                $('#hidTotalRecords').val(result[0].ProductCount);

                htmlText = "";

                for (var key in result) {

                    var datacatAttribute = '';
                    var classMixit = '';

                    if (blnIsGoogleMixIt == 'True') {
                        if (result[key].GoogleMixitSectionNames == null) {
                            result[key].GoogleMixitSectionNames = '';
                        }

                        datacatAttribute = ' data-cat="' + result[key].AsLowAsPrice + '" ';
                        classMixit = ' mix-target ' + result[key].GoogleMixitSectionNames;
                    }

                    if (isBothListAndGridEnabled == 'True') {
                        if ($("#alstView").hasClass("hidden")) {
                            htmlText += '<li id="liProducts" ' + datacatAttribute + ' class="' + classMixit + '" >';
                        }
                        else {
                            htmlText += '<li id="liProducts" class="col-lg-4 col-md-4 col-sm-6 ' + classMixit + '" ' + datacatAttribute + '>';
                        }
                    }
                    else if (isListViewDefault == 'True') {
                        htmlText += '<li id="liProducts" ' + datacatAttribute + ' class="' + classMixit + '" >';
                    }
                    else {
                        htmlText += '<li id="liProducts" class="col-lg-4 col-md-4 col-sm-6 ' + classMixit + '" ' + datacatAttribute + '>';
                    }

                    htmlText += '<div class="prod_list_cell">';
                    htmlText += '<div class="prod_list_image_outer">';
                    htmlText += '<a href=' + result[key].productDetailURL + ' id="aProductImage" class="indeedProductImage" style="position:relative">';
                    htmlText += '<img src=' + result[key].DefaultImageName + ' id="imgProduct" class="prod_list_image_inner"></a>';

                    if (IsShowSectionIcons == 'True' && result[key].SectionImageData != null) {
                        htmlText += '<div id="SectionIcons" class="product_list_icon_row"><img title=' + result[key].SectionTitle + ' alt=' + result[key].SectionAlt + ' src=' + result[key].SectionImageData + ' class="product_list_icon"></div>';
                    }

                    htmlText += '</div>';
                    htmlText += '<div class="prod_list_text_outer">';
                    htmlText += '<a href=' + result[key].productDetailURL + ' id="NDProductList">';
                    htmlText += '<div id="aProductName" class="prod_list_text_title productSmlName">' + result[key].ProductName + '</div>'
                    htmlText += '<div class="prod_list_text_code productCode"> ' + result[key].ProductCode + '</div>';
                    htmlText += '</a>';
                    htmlText += '<div class="prod_list_text_price productPrice">';

                    if (result[key].AsLowAsstrikePrice != null && result[key].AsLowAsstrikePrice != "0") {
                        if (result[key].IsStrikeNowWas != null && result[key].IsStrikeNowWas == true) {
                            if (IsOnlyCurrency != null && IsOnlyCurrency == true) {
                                if (isEuroPositionChanged == 'True') {
                                    htmlText += '<div id="spnPrice" class="price_was col-sm-12">' + result[key].finalProductStrikePriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                                    htmlText += '<div id="spnStrikePrice" class="price_now col-sm-12">' + result[key].finalProductPriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                                }
                                else {
                                    htmlText += '<div id="spnPrice" class="price_was col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductStrikePriceForDisplay + '</div>';
                                    htmlText += '<div id="spnStrikePrice" class="price_now col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductPriceForDisplay + '</div>';
                                }
                            }
                            else if (IsOnlyPoints != null && IsOnlyPoints == true) {
                                htmlText += '<div id="spnPoint" class="price_was col-sm-12">' + result[key].finalProductStrikePointsForDisplay + '</div>';
                                htmlText += '<div id="spnStrikePoint" class="price_now col-sm-12">' + result[key].finalProductPointsForDisplay + '</div>';
                            }
                            else if (IsBothPointsAndCurrency != null && IsBothPointsAndCurrency == true) {
                                if (isEuroPositionChanged == 'True') {
                                    htmlText += '<div id="spnPrice" class="price_was col-sm-12">' + result[key].finalProductStrikePriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                                    htmlText += '<div id="spnStrikePrice" class="price_now col-sm-12">' + result[key].finalProductPriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                                }
                                else {
                                    htmlText += '<div id="spnPrice" class="price_was col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductStrikePriceForDisplay + '</div>';
                                    htmlText += '<div id="spnStrikePrice" class="price_now col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductPriceForDisplay + '</div>';
                                }
                                htmlText += '<div id="spnApprox" class="cls_Approx">' + approxText + '</div>';
                                htmlText += '<div id="spnPoint" class="price_was col-sm-12">' + result[key].finalProductStrikePointsForDisplay + '</div>';
                                htmlText += '<div id="spnStrikePoint" class="price_now col-sm-12">' + result[key].finalProductPointsForDisplay + '</div>';
                            }
                        }
                        else {
                            if (IsOnlyCurrency != null && IsOnlyCurrency == true) {
                                if (isEuroPositionChanged == 'True') {
                                    htmlText += '<div id="spnPrice" class="price_was col-sm-12">' + result[key].finalProductPriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                                    htmlText += '<div id="spnStrikePrice" class="price_now col-sm-12">' + result[key].finalProductStrikePriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                                }
                                else {
                                    htmlText += '<div id="spnPrice" class="price_was col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductPriceForDisplay + '</div>';
                                    htmlText += '<div id="spnStrikePrice" class="price_now col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductStrikePriceForDisplay + '</div>';
                                }
                            }
                            else if (IsOnlyPoints != null && IsOnlyPoints == true) {
                                htmlText += '<div id="spnPoint" class="price_was col-sm-12">' + result[key].finalProductStrikePointsForDisplay + '</div>';
                                htmlText += '<div id="spnStrikePoint" class="price_now col-sm-12">' + result[key].finalProductPointsForDisplay + '</div>';
                            }
                            else if (IsBothPointsAndCurrency != null && IsBothPointsAndCurrency == true) {
                                if (isEuroPositionChanged == 'True') {
                                    htmlText += '<div id="spnPrice" class="price_was col-sm-12">' + result[key].finalProductPriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                                    htmlText += '<div id="spnStrikePrice" class="price_now col-sm-12">' + result[key].finalProductStrikePriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                                }
                                else {
                                    htmlText += '<div id="spnPrice" class="price_was col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductPriceForDisplay + '</div>';
                                    htmlText += '<div id="spnStrikePrice" class="price_now col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductStrikePriceForDisplay + '</div>';
                                }
                                htmlText += '<div id="spnApprox" class="cls_Approx">' + approxText + '</div>';
                                htmlText += '<div id="spnPoint" class="price_was col-sm-12">' + result[key].finalProductStrikePointsForDisplay + '</div>';
                                htmlText += '<div id="spnStrikePoint" class="price_now col-sm-12">' + result[key].finalProductPointsForDisplay + '</div>';
                            }
                        }
                    }
                    else if (result[key].AsLowAsPrice != null && result[key].AsLowAsPrice != "0") {
                        if (IsOnlyCurrency != null && IsOnlyCurrency == true) {
                            if (isEuroPositionChanged == 'True') {
                                htmlText += '<div id="spnPrice" class="price col-sm-12">' + result[key].finalProductPriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                            }
                            else {
                                htmlText += '<div id="spnPrice" class="price col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductPriceForDisplay + '</div>';
                            }
                        }
                        else if (IsOnlyPoints != null && IsOnlyPoints == true) {
                            htmlText += '<div id="spnPoint" class="price col-sm-12">' + result[key].finalProductPointsForDisplay + '</div>';
                        }
                        else if (IsBothPointsAndCurrency != null && IsBothPointsAndCurrency == true) {
                            if (isEuroPositionChanged == 'True') {
                                htmlText += '<div id="spnPrice" class="price col-sm-12">' + result[key].finalProductPriceForDisplay + $("#hidCurrencySymbol").val() + '</div>';
                            }
                            else {
                                htmlText += '<div id="spnPrice" class="price col-sm-12">' + $("#hidCurrencySymbol").val() + result[key].finalProductPriceForDisplay + '</div>';
                            }

                            htmlText += '<div id="spnApprox" class="cls_Approx">' + approxText + '</div>';
                            htmlText += '<div id="spnPoint" class="price col-sm-12">' + result[key].finalProductPointsForDisplay + '</div>';
                        }                  
                    }
                    htmlText += '<input name="hdniscallforprice" type="hidden" id="hdniscallforprice" value="False">'
                    htmlText += '<div id="spnVatValueText" style="display:none">* (includes -1 % VAT)</div>';
                    htmlText += '<div id="divProductvariantsCSV" class="variantsCSV hidden"></div>';
                    htmlText += '</div>';

                    if (isStocksEnabled == 'True') {
                        htmlText += '<div id="spnstock" class="prod_list_text_price productPrice"> ' + result[key].finalStockDetails + '</div>';
                    }

                    htmlText += '<div id="dvDescription" class="prod_list_text_desc productDescription" style="display: block;">';
                    htmlText += result[key].ProductDescription;
                    htmlText += '</div>';

                    if (isProductComparisonEnabled == 'True') {
                    htmlText += '<div class="compareContainer ">';
                        htmlText += '<label id="lblProductComparison" class="prod_list_checkbox" runat="server" visible="false">';
                        htmlText += '<span class="comparecheckboxgrid">';
                        htmlText += '<input id="chkCompare"  runat="server" ClientIdMode="static" type="checkbox">';
                        htmlText += '</span>';
                        htmlText += '<span id="spnCompareText" runat="server"></span>';
                        htmlText += '</label>';
                    htmlText += '</div>';
                    }
                    if (isQuickViewEnabled == 'True') {
                        htmlText += '<div class="prod_list_button">';
                        htmlText += '<a href="javascript:void(0);" id="aQuickView" ProductId=' + result[key].ProductId + ' ClientIDmode="static"  class="btn btn-primary customActionBtn">';
                        htmlText += quickViewText + '</a></div>';
                    }

                    htmlText += '</div>';
                    htmlText += '</div>';
                    htmlText += '</li>';
                }

                $('#dvProductData').append(htmlText);
                if (blnIsInfiniteScroll == 'True') {
                    var status = true;
                    //if ($('#hidPageNo').val() == 1) {
                        $(window).scroll(function () {
                            var footerHeight = ($('#footer').height() / 2) + 900;
                            if ($(window).scrollTop() + footerHeight >= ($(document).height()) - ($(window).height())) {
                                if (status == 'true' || status == true) {
                                    status = false;
                                    $('#hidPageNo').val(parseInt($('#hidPageNo').val()) + 1);
                                    GetProducts();
                                }
                            }
                        });
                    //}
                }
                else if (isPaginationEnabled == 'True') {
                    Paging($('#hidPageNo').val(), PageSize, $('#hidTotalRecords').val());
                }

                 if (IsBindFilterData) {
                     if ((showfilters == 'True') || (blnIsGoogleMixIt == 'True')) {
                         BindFilterProduct();
                         if (blnIsGoogleMixIt == 'True') {
                             $('.PaginationContainer ').removeClass('col-md-5');
                             $('.PaginationContainer ').addClass('col-md-10');
                             $('#aMobileFilter').hide();
                         }
                     }
                     else {
                         $('#dvLoader').hide();
                     }
                 }

                if (($('#hidPageNo').val() == 1 || $('#hidPageNo').val() == 0)
                        && SearchProductIds != '' && SearchProductIds != '0')
                {
                    $('#dvSearchMessage').html(ResultsFoundMessage.replace("{keyword}", ' "' + strSearchKeyword + '"').replace("{count}", $('#hidTotalRecords').val()));
                    $('#dvSearchMessage').show();
                }
            }
            $("#wait").css("display", "none");
        }
    });
}

var ConvertTextToHTML = function (inputText) {
    return $("<span />", { html: inputText }).text();
};

function BindFilterProduct() {
    var parameters = {

        CategoryName: $('#hidcategoryName').val(),
        SubCategoryName: $('#hidsubCategoryName').val(),
        SubSubCategoryName: $('#hidsubSubCategoryName').val(),
        SectionName: $('#hidsectionName').val(),
        MinPrice: MiniumPrice,
        MaxPrice: MaximumPrice,
        SectionIds: $('#hidSectionIds').val(),
        Colors: $('#hidColorIds').val(),
        CustomFilter1: $('#hidCustomFilter1').val(), //Size
        CustomFilter2: $('#hidCustomFilter2').val(), //Time
        CustomFilter3: $('#hidCustomFilter3').val(), //FILTER1
        CustomFilter4: $('#hidCustomFilter4').val(), //FILTER2
        CustomFilter5: $('#hidCustomFilter5').val(),  //FILTER3
        SearchProductIds: SearchProductIds
    };

    $.ajax({
        type: 'POST',
        url: host + '/ProductListing/BindFilterProductJson',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(parameters),
        cache: false,
        async: true,
        success: function (result) {
            var divHtml = '';
            if (blnIsGoogleMixIt == 'True') {
                divHtml += ' <div class="dvsections col-md-6"> ';
                divHtml += ' <a class="filter-btn" data-filter="all"> ';
                divHtml += ' <img title="All" alt="all" src="' + allImageSource + '" class="SectionIcon"> ';
                divHtml += ' </a> ';

                if (result.ProductSectionFilter != null
                        && result.ProductSectionFilter.length > 0) {

                    $.each(result.ProductSectionFilter, function (index, eachItem) {
                        if (eachItem.IsImageExists == true) {
                            divHtml += ' <a class="filter-btn" data-filter="' + eachItem.SectionName + '"> ';
                            divHtml += ' <img title="' + eachItem.SectionName + '" ';
                            divHtml += ' alt="' + eachItem.SectionName + '" ';
                            divHtml += ' src="' + host + 'admin/Images/Section/Icon/' + eachItem.SectionId + eachItem.IconImageExtension + '" ';
                            divHtml += ' class="SectionIcon">';
                            divHtml += ' </a>';
                        }
                        else {
                            divHtml += ' <a class="filter-btn" data-filter="' + eachItem.SectionName + '"> ';
                            divHtml += eachItem.SectionName;
                            divHtml += '  </a>';
                        }
                    });
                }
                divHtml += ' </div> ';
                divHtml += ' <div class="dvpricefilter priceFilterBtn col-md-6"> ';
                divHtml += ' <input data-sort="data-cat" data-order="desc" ';
                divHtml += ' value=' + Price_Lowest_first + ' type="button" ';
                divHtml += ' class="sort active btn btn-sm btn-primary customActionBtn" /> ';
                divHtml += ' <input data-sort="data-cat" data-order="asc" ';
                divHtml += ' type="button" ';
                divHtml += ' value=' + Price_highest_first;
                divHtml += '  class="sort btn btn-sm btn-primary list_button customActionBtn" />';
                divHtml += '  </div>';

                $('#dvSectionIconContainer').html(divHtml);
            }
            else {

                divHtml += ' <div class="panel-group" id="divFilter" role="tablist" aria-multiselectable="true"> ';

                //---PRICE FILTER----
                if (result.FilterProductInfo.ShowPriceFilter == true
                    && result.ProductPriceFilter != null
                    && result.ProductPriceFilter.length > 0) {
                    divHtml += '  <div class="panel customPanel panel-default" >';
                    divHtml += '  <a role="button" class="collapsed" data-toggle="collapse" data-target="#Price" aria-expanded="true">';
                    divHtml += '  <div class="panel-heading customAccordions panel-heading-a" role="tab" id="headingOne">';
                    divHtml += '  <h4 class="panel-title customAccordionsText" id="hPrice">' + Generic_Price_Title_Text + '</h4>';
                    divHtml += '  </div>';
                    divHtml += '  </a>';
                    divHtml += '  <div id="Price" class="panel-collapse collapse in" aria-expanded="true" aria-labelledby="headingOne">';
                    divHtml += '  <div class="panel-body">';
                    divHtml += '  <div class="SliderRange text-center">';
                    divHtml += '  <p class="pageSmlText"><span id="pPriceRange"> ' + result.FilterProductInfo.PriceRangeFormattedText + '</span></p>';
                    divHtml += '  <input id="priceRange" type="text" class="span2" value=""';
                    divHtml += '  data-slider-min=' + result.FilterProductInfo.Price_DataSliderMin_FormattedText;
                    divHtml += '  data-slider-max=' + result.FilterProductInfo.Price_DataSliderMax_FormattedText;
                    divHtml += '   data-slider-step="0.05"';
                    divHtml += '  data-slider-value=' + result.FilterProductInfo.Price_DataSliderValue_FormattedText + ' />';
                    divHtml += '  <div class="row">';
                    divHtml += '  <b id="bMinPrice" class="pageSmlText text-left col-md-5 col-sm-5 col-sm-offset-1 col-xs-offset-0 col-xs-6">' + result.FilterProductInfo.MinPriceFormattedText + '</b>';
                    divHtml += ' <b id="bMaxPrice" class="pageSmlText text-right col-md-5 col-sm-5 col-xs-6">' + result.FilterProductInfo.MaxPriceFormattedText + '</b>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //---END - PRICE FILTER----

                //---SUB CATEGORY--
                if (result.FilterProductInfo.ShowSubCategoryFilter == true
                        && result.ProductCategoryFilter != null
                        && result.ProductCategoryFilter.length > 0) {

                    divHtml += ' <div class="panel customPanel panel-default" id="dvSubCategoryFilter">';
                    divHtml += ' <a role="button" class="collapsed" aria-controls="SubCategory" href="#SubCategory" data-toggle="collapse" aria-expanded="false">';
                    divHtml += ' <div class="panel-heading customAccordions panel-heading-a" role="tab">';
                    divHtml += ' <h4 class="panel-title customAccordionsText" id="hSubCategory">' + SubCategory_Title + '</h4>';
                    divHtml += ' </div>';
                    divHtml += ' </a>';
                    divHtml += ' <div id="SubCategory" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">';
                    divHtml += ' <div class="panel-body">';

                    var parentHrefLink = '';
                    var parentInnerHtml = '';
                    var hidsubSubCategoryName = document.getElementById('hidsubSubCategoryName').value;
                    var hidsubCategoryName = document.getElementById('hidsubCategoryName').value;
                    var hidcategoryName = document.getElementById('hidcategoryName').value;

                    if (hidsubSubCategoryName == null
                        || hidsubSubCategoryName == "") {

                        if (hidsubCategoryName != null
                            && hidsubCategoryName != "") {
                            parentInnerHtml = hidcategoryName;
                            parentHrefLink = host + "SubCategories/" + encodedCategoryName;
                        }
                    }
                    else {
                        parentInnerHtml = hidsubCategoryName;
                        parentHrefLink = host + "SubCategories/" + encodedCategoryName + "/" + encodedSubCategoryName;
                    }

                    divHtml += ' <a id="aParentCategory" class="pageText" href="' + parentHrefLink + '">' + parentInnerHtml + '</a>';

                    $.each(result.ProductCategoryFilter, function (index, eachCategory) {
                        var hrefLink = '';
                        var className = "hyperLinkSml";

                        if (hidsubSubCategoryName == null
                                || hidsubSubCategoryName == "") {

                            if (hidsubCategoryName != null
                                && hidsubCategoryName != "") {

                                if (hidsubCategoryName.toLowerCase() == eachCategory.CategoryName.toLowerCase()) {
                                    className = "active hyperLinkSml";
                                }
                                hrefLink = host + "SubCategory/" + encodedCategoryName + "/" + eachCategory.EncodedCategoryName;
                            }
                        }
                        else {
                            if (hidsubSubCategoryName.toLowerCase() == eachCategory.CategoryName.toLowerCase()) {
                                className = "active hyperLinkSml";
                            }
                            hrefLink = host + "SubCategory/" + encodedCategoryName + "/" + encodedSubCategoryName + "/" + eachCategory.EncodedCategoryName;
                        }

                        divHtml += ' <div class="filter_row">';
                        divHtml += ' <a id="aCategory" class="' + className + '" href="' + hrefLink + '"> ';
                        divHtml += eachCategory.CategoryName + '&nbsp;(' + eachCategory.ProductCount + ')';
                        divHtml += ' </a>';
                        divHtml += ' </div>';
                    });

                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //---END SUB CATEGORY-->

                //-----COLOR FILTER --
                if (result.FilterProductInfo.ShowColorFilter == true
                        && result.ProductColorFilter != null
                        && result.ProductColorFilter.length > 0) {
                    divHtml += ' <div class="panel customPanel panel-default" id="dvColorFilter">';
                    divHtml += ' <a role="button" class="collapsed" data-toggle="collapse" data-target="#Color" aria-expanded="true">';
                    divHtml += ' <div class="panel-heading customAccordions panel-heading-a" role="tab" id="headingTwo">';
                    divHtml += ' <h4 class="panel-title customAccordionsText" id="hColor">' + Color_Title + '</h4>';
                    divHtml += ' </div>';
                    divHtml += ' </a>';
                    divHtml += ' <div id="Color" class="panel-collapse collapse" aria-expanded="true">';
                    divHtml += ' <div class="panel-body">';

                    var existingColorIds = "," + $('#hidColorIds').val();

                    $.each(result.ProductColorFilter, function (index, eachColor) {

                        var isColorChecked = '';

                        if (existingColorIds.indexOf("," + eachColor.ColorId + ",") > -1) {
                            isColorChecked = " checked ";
                        }

                        divHtml += ' <div class="filter_row">';
                        divHtml += ' <label class="customLabel">';
                        divHtml += ' <input id="chkColorName" ClientIdMode="static" class="classColorName"';
                        divHtml += ' type="checkbox" value="' + eachColor.ColorId + '" ' + isColorChecked + '> ';
                        divHtml += eachColor.ColorName + '&nbsp;(' + eachColor.ProductCount + ')';
                        divHtml += ' </label>';
                        divHtml += ' </div>';

                    });

                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //----END COLOR FILTER --

                //------- SECTION FILTER --------
                if (result.FilterProductInfo.ShowSectionFilter == true
                        && result.ProductSectionFilter != null
                        && result.ProductSectionFilter.length > 0) {
                    divHtml += ' <div class="panel customPanel panel-default">';
                    divHtml += ' <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#Section" aria-expanded="false">';
                    divHtml += ' <div class="panel-heading customAccordions panel-heading-a" role="tab">';
                    divHtml += ' <h4 class="panel-title customAccordionsText" id="hSection">' + Section_Title + '</h4>';
                    divHtml += ' </div>';
                    divHtml += ' </a>';
                    divHtml += ' <div id="Section" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">';
                    divHtml += ' <div class="panel-body">';

                    var existingSectionIds = "," + $('#hidSectionIds').val();

                    $.each(result.ProductSectionFilter, function (index, eachSection) {

                        var isSectionChecked = '';

                        if (existingSectionIds.indexOf("," + eachSection.SectionId + ",") > -1) {
                            isSectionChecked = " checked ";
                        }

                        divHtml += ' <div class="filter_row">';
                        divHtml += ' <label class="customLabel">';
                        divHtml += ' <input ClientIdMode="static" class="classSection" type="checkbox" value="' + eachSection.SectionId + '" ' + isSectionChecked + '> ';
                        divHtml += eachSection.SectionName + '&nbsp;(' + eachSection.ProductCount + ')';
                        divHtml += ' </label>';
                        divHtml += ' </div>';

                    });

                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //------END - SECTION FILTER -----

                //-------SIZE FILTER -----
                if (result.FilterProductInfo.ProductFilterSizeList != null
                        && result.FilterProductInfo.ProductFilterSizeList.length > 0) {
                    divHtml += ' <div class="panel customPanel panel-default" id="dvCustomFilter1">';
                    divHtml += ' <a role="button" class="collapsed" data-toggle="collapse" data-target="#Filter1" aria-expanded="true">';
                    divHtml += ' <div class="panel-heading customAccordions panel-heading-a" role="tab">';
                    divHtml += ' <h4 class="panel-title customAccordionsText" id="hCustomFilter1">' + result.ProductCustomFilter1[0].FilterName + '</h4>';
                    divHtml += ' </div>';
                    divHtml += ' </a>';
                    divHtml += ' <div id="Filter1" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">';
                    divHtml += ' <div class="panel-body">';

                    var existingSizeIds = "|" + $('#hidCustomFilter1').val();

                    $.each(result.FilterProductInfo.ProductFilterSizeList, function (index, eachItem) {

                        var isSizeChecked = '';

                        if (existingSizeIds.indexOf("|" + eachItem.FilterData + "|") > -1) {
                            isSizeChecked = " checked ";
                        }

                        divHtml += ' <div class="filter_row">';
                        divHtml += ' <label class="customLabel">';
                        divHtml += ' <input id="chkCustomFilter1" ClientIdMode="static" class="classSize" type="checkbox" value="' + eachItem.FilterData + '" ' + isSizeChecked + '> ';
                        divHtml += eachItem.FilterData + '&nbsp;(' + eachItem.ProductCount + ')';
                        divHtml += ' </label>';
                        divHtml += ' </div>';

                    });

                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //----END SIZE FILTER ----

                //--PRODUCTION TIME FILTER -->
                if (result.FilterProductInfo.ProductFilterTimeList != null
                        && result.FilterProductInfo.ProductFilterTimeList.length > 0) {
                    divHtml += ' <div class="panel customPanel panel-default" id="dvCustomFilter2">';
                    divHtml += ' <a role="button" class="collapsed" data-toggle="collapse" data-target="#Filter2" aria-expanded="false">';
                    divHtml += ' <div class="panel-heading customAccordions panel-heading-a" role="tab">';
                    divHtml += ' <h4 class="panel-title customAccordionsText" id="hCustomFilter2">' + result.ProductCustomFilter2[0].FilterName + '</h4>';
                    divHtml += ' </div>';
                    divHtml += ' </a>';
                    divHtml += ' <div id="Filter2" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">';
                    divHtml += ' <div class="panel-body">';

                    var existingTimeIds = "|" + $('#hidCustomFilter2').val();

                    $.each(result.FilterProductInfo.ProductFilterTimeList, function (index, eachItem) {

                        var isTimeChecked = '';

                        if (existingTimeIds.indexOf("|" + eachItem.FilterData + "|") > -1) {
                            isTimeChecked = " checked ";
                        }

                        divHtml += ' <div class="filter_row">';
                        divHtml += ' <label class="customLabel">';
                        divHtml += ' <input id="chkCustomFilter2" ClientIdMode="static" class="classTime" type="checkbox" value="' + eachItem.FilterData + '" ' + isTimeChecked + '> ';
                        divHtml += eachItem.FilterData + '&nbsp;(' + eachItem.ProductCount + ')';
                        divHtml += ' </label>';
                        divHtml += ' </div>';

                    });

                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //--END PRODUCTION TIME FILTER --

                //--FILTER 1 -
                if (result.FilterProductInfo.ProductFilterFILTER1List != null
                        && result.FilterProductInfo.ProductFilterFILTER1List.length > 0) {
                    divHtml += ' <div class="panel customPanel panel-default" id="dvCustomFilter3">';
                    divHtml += ' <a role="button" class="collapsed" data-toggle="collapse" data-target="#Filter3" aria-expanded="true">';
                    divHtml += ' <div class="panel-heading customAccordions panel-heading-a" role="tab">';
                    divHtml += ' <h4 class="panel-title customAccordionsText" id="hCustomFilter3">' + result.ProductCustomFilter3[0].FilterName + '</h4>';
                    divHtml += ' </div>';
                    divHtml += ' </a>';
                    divHtml += ' <div id="Filter3" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">';
                    divHtml += ' <div class="panel-body">';

                    var existingFilter1Ids = "|" + $('#hidCustomFilter3').val();

                    $.each(result.FilterProductInfo.ProductFilterFILTER1List, function (index, eachItem) {

                        var isFilter1Checked = '';

                        if (existingFilter1Ids.indexOf("|" + eachItem.FilterData + "|") > -1) {
                            isFilter1Checked = " checked ";
                        }

                        divHtml += ' <div class="filter_row">';
                        divHtml += ' <label class="customLabel">';
                        divHtml += ' <input id="chkCustomFilter3" ClientIdMode="static" class="classFILTER1" type="checkbox" value="' + eachItem.FilterData + '" ' + isFilter1Checked + '> ';
                        divHtml += eachItem.FilterData + '&nbsp;(' + eachItem.ProductCount + ')';
                        divHtml += ' </label>';
                        divHtml += ' </div>';

                    });

                    divHtml += ' </div>';
                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //--END FILTER 1 -

                //--FILTER 2 -
                if (result.FilterProductInfo.ProductFilterFILTER2List != null
                        && result.FilterProductInfo.ProductFilterFILTER2List.length > 0) {
                    divHtml += ' <div class="panel customPanel panel-default" id="dvCustomFilter4">';
                    divHtml += ' <a role="button" class="collapsed" data-toggle="collapse" data-target="#Filter4" aria-expanded="true">';
                    divHtml += ' <div class="panel-heading customAccordions panel-heading-a" role="tab">';
                    divHtml += ' <h4 class="panel-title customAccordionsText" id="hCustomFilter4">' + result.ProductCustomFilter4[0].FilterName + '</h4>';
                    divHtml += ' </div>';
                    divHtml += ' </a>';
                    divHtml += ' <div id="Filter4" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">';

                    var existingFilter2Ids = "|" + $('#hidCustomFilter4').val();

                    $.each(result.FilterProductInfo.ProductFilterFILTER2List, function (index, eachItem) {

                        var isFilter2Checked = '';

                        if (existingFilter2Ids.indexOf("|" + eachItem.FilterData + "|") > -1) {
                            isFilter2Checked = " checked ";
                        }

                        divHtml += ' <div class="filter_row">';
                        divHtml += ' <label class="customLabel">';
                        divHtml += ' <input id="chkCustomFilter4" ClientIdMode="static" class="classFILTER2" type="checkbox" value="' + eachItem.FilterData + '" ' + isFilter2Checked + '> ';
                        divHtml += eachItem.FilterData + '&nbsp;(' + eachItem.ProductCount + ')';
                        divHtml += ' </label>';
                        divHtml += ' </div>';

                    });

                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //--END FILTER 2 --

                //--FILTER 3 --
                if (result.FilterProductInfo.ProductFilterFILTER3List != null
                    && result.FilterProductInfo.ProductFilterFILTER3List.length > 0) {
                    divHtml += ' <div class="panel customPanel panel-default" id="dvCustomFilter5">';
                    divHtml += ' <a role="button" class="collapsed" data-toggle="collapse" data-target="#Filter5" aria-expanded="true">';
                    divHtml += ' <div class="panel-heading customAccordions panel-heading-a" role="tab">';
                    divHtml += ' <h4 class="panel-title customAccordionsText" id="hCustomFilter5">' + result.ProductCustomFilter5[0].FilterName + '</h4>';
                    divHtml += ' </div>';
                    divHtml += ' </a>';
                    divHtml += ' <div id="Filter5" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">';

                    var existingFilter3Ids = "|" + $('#hidCustomFilter5').val();

                    $.each(result.FilterProductInfo.ProductFilterFILTER3List, function (index, eachItem) {

                        var isFilter3Checked = '';

                        if (existingFilter3Ids.indexOf("|" + eachItem.FilterData + "|") > -1) {
                            isFilter3Checked = " checked ";
                        }

                        divHtml += ' <div class="filter_row">';
                        divHtml += ' <label class="customLabel">';
                        divHtml += ' <input id="chkCustomFilter5" ClientIdMode="static" class="classFILTER3" type="checkbox" value="' + eachItem.FilterData + '" ' + isFilter3Checked + '> ';
                        divHtml += eachItem.FilterData + '&nbsp;(' + eachItem.ProductCount + ')';
                        divHtml += ' </label>';
                        divHtml += ' </div>';

                    });

                    divHtml += ' </div>';
                    divHtml += ' </div>';
                }
                //--END FILTER 3 ---

                divHtml += ' </div>';

                $('#dvFilterData').html(divHtml);

                BindEvents();
            }
        }
    });
}

function Paging(PageNo, PageSize, TotalRecords) {
    try {
        intTotalPages = TotalRecords;
        $('#totalitems').html(TotalRecords);
        if (PageNo == 0) {
            $('#dvPagination').hide();
            $('#dvPageNo').hide();
            $('#records').hide();
            $('#aViewAll').hide();

            $('#dvPaginationBottom').hide();
            $('#dvPageNoBottom').hide();
            $('#recordsBottom').hide();
            $('#aViewAllBottom').hide();
        }
        else {
            var intFromRecord = ((parseInt(PageNo) - 1) * PageSize) + 1;
            var intToRecord = parseInt(PageNo) * (PageSize);
            //var intTotalPages = 0;

            if (intToRecord > TotalRecords) {
                intToRecord = TotalRecords;
            }
            intTotalPages = Math.ceil(TotalRecords / PageSize);

            $('#records').html(intFromRecord + " - " + intToRecord + " of " + TotalRecords);
            $('#recordsBottom').html(intFromRecord + " - " + intToRecord + " of " + TotalRecords);

            $('#currentpage').val(PageNo);
            $('#spnTotalPageCount').html(intTotalPages);
            $('#currentpageBottom').val(PageNo);
            $('#spnTotalPageCountBottom').html(intTotalPages);

            if (intTotalPages == 1) {
                $('#aViewAll').hide();
                $('#dvPagination').hide();
                $('#dvPageNo').hide();
                $('#records').hide();

                $('#aViewAllBottom').hide();
                $('#dvPaginationBottom').hide();
                $('#dvPageNoBottom').hide();
                $('#recordsBottom').hide();
            }
            else {
                $('#aViewAll').show();
                $('#dvPagination').show();
                $('#dvPageNo').show();
                $('#records').show();

                $('#aViewAllBottom').show();
                $('#dvPaginationBottom').show();
                $('#dvPageNoBottom').show();
                $('#recordsBottom').show();
            }

            if (parseInt(PageNo) == 1) {
                $('#aFirst').addClass("disabled");
                $('#aFirst').removeClass("disabled");

                $('#aFirstBottom').addClass("disabled");
                $('#aFirstBottom').removeClass("disabled");
            }
            else {
                $('#aFirst').attr("onclick", "BindPage(1)");
                $('#aPrev').removeAttr("onclick");

                $('#aFirstBottom').attr("onclick", "BindPage(1)");
                $('#aPrevBottom').removeAttr("onclick");
            }
            if (parseInt(PageNo) > 1) {
                $('#aPrev').removeClass("disabled");
                $('#aPrev').attr("onclick", "BindPage(" + (parseInt(PageNo) - 1) + ")");

                $('#aPrevBottom').removeClass("disabled");
                $('#aPrevBottom').attr("onclick", "BindPage(" + (parseInt(PageNo) - 1) + ")");
            }
            else {
                $('#aPrev').addClass("disabled");
                $('#aPrev').removeAttr("onclick");

                $('#aPrevBottom').addClass("disabled");
                $('#aPrevBottom').removeAttr("onclick");
            }
            if (parseInt(PageNo) == intTotalPages) {
                $('#aNext').addClass("disabled");
                $('#aNext').removeAttr("onclick");

                $('#aNextBottom').addClass("disabled");
                $('#aNextBottom').removeAttr("onclick");
            }
            else {
                $('#aNext').removeClass("disabled");
                $('#aNext').attr("onclick", "BindPage(" + (parseInt(PageNo) + 1) + ")");

                $('#aNextBottom').removeClass("disabled");
                $('#aNextBottom').attr("onclick", "BindPage(" + (parseInt(PageNo) + 1) + ")");
            }
            if (parseInt(PageNo) == intTotalPages) {
                $('#aLast').addClass("disabled");
                $('#aLast').removeAttr("onclick");

                $('#aLastBottom').addClass("disabled");
                $('#aLastBottom').removeAttr("onclick");
            }
            else {
                $('#aLast').removeClass("disabled");
                $('#aLast').attr("onclick", "BindPage(" + intTotalPages + ")");

                $('#aLastBottom').removeClass("disabled");
                $('#aLastBottom').attr("onclick", "BindPage(" + intTotalPages + ")");
            }
        }
    } catch (e) {

    }
}

function BindPage(NewPageNo) {
    $('#hidPageNo').val(NewPageNo);
    GetProducts();
}

function ViewAllProducts() {
    $('#hidPageNo').val(0);
    GetProducts();
}

function EnterEvent(e) {
    if (e.keyCode == 13) {
        JumpToPage();
        e.preventDefault();
    }
}

function EnterEventBottom(e) {
    if (e.keyCode == 13) {
        JumpToPageBottom();
        e.preventDefault();
    }
}

function JumpToPage() {
    var jumpTo = $('#currentpage').val();
    if (jumpTo != '' && !isNaN(jumpTo)) {
        if (jumpTo < 1) {
            jumpTo = 1;
        }
        if (jumpTo > intTotalPages) {
            jumpTo = intTotalPages;
        }
        $('#hidPageNo').val(jumpTo);
        GetProducts();
    }
}

function JumpToPageBottom() {
    var jumpTo = $('#currentpageBottom').val();
    if (jumpTo != '' && !isNaN(jumpTo)) {
        if (jumpTo < 1) {
            jumpTo = 1;
        }
        if (jumpTo > intTotalPages) {
            jumpTo = intTotalPages;
        }
        $('#hidPageNo').val(jumpTo);
        GetProducts();
    }
}

function BindRandomProducts() {
    $.ajax({
        type: 'POST',
        url: host + '/ProductListing/BindRandomProductsJson',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: null,
        success: function (result) {
            var divHtml = '';
            $.each(result, function (index, item) {
                divHtml += '<a id="aProductImage" href="' + item.RandomProductInfo.ProductImageHref + '">';
                divHtml += '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">';
                divHtml += '<div class="prod_list_cell">';
                divHtml += '<div class="prod_list_image_outer">';
                divHtml += '<img class="prod_list_image_inner" src="' + item.RandomProductInfo.ProductImageSrc + '" id="imgProduct" >';
                divHtml += '</div>';
                divHtml += '<div class="prod_list_text_outer">';
                divHtml += '<div class="prod_list_text_title productSmlName" id="aProductName" > ' + item.RandomProductInfo.ProductNameText + ' </div>';
                divHtml += '<div class="prod_list_text_code productCode">' + item.ProductCode + '</div>';
                divHtml += '<div id="spnPrice" class="prod_list_text_price productPrice">' + item.RandomProductInfo.PriceFormattedText + '</div>';
                divHtml += '<div class="prod_list_text_desc productDescription" id="dvDescription" ></div>';
                divHtml += '</div>';
                divHtml += '</div>';
                divHtml += '</div>';
                divHtml += '</a>';
            });
            $('#divBindRandomProducts').html(divHtml);
        }
    });
}

function BindEvents() {

    OnPriceChange();

    BindCheckChangeEvent('.classColorName', '#hidColorIds', ',');

    BindCheckChangeEvent('.classSection', '#hidSectionIds', ',');

    BindCheckChangeEvent('.classSize', '#hidCustomFilter1', '|');

    BindCheckChangeEvent('.classTime', '#hidCustomFilter2', '|');

    BindCheckChangeEvent('.classFILTER1', '#hidCustomFilter3', '|');

    BindCheckChangeEvent('.classFILTER2', '#hidCustomFilter4', '|');

    BindCheckChangeEvent('.classFILTER3', '#hidCustomFilter5', '|');

    OnResetFilter();
}

function OnResetFilter() {

    $('#resetFilters').unbind('click');

    $('#resetFilters').click(function () {
        $('#hidPageNo').val("1");
        $('#hidColorIds').val("");
        $('#hidSectionIds').val("");
        $('#hidCustomFilter1').val("");
        $('#hidCustomFilter2').val("");
        $('#hidCustomFilter3').val("");
        $('#hidCustomFilter4').val("");
        $('#hidCustomFilter5').val("");
        MiniumPrice = 0;
        MaximumPrice = 0;
        $('#dvProductData').html('');
        htmlText = "";
        GetProducts();
    });
}

function OnPriceChange() {
    if ($("#priceRange").length > 0) {
        $('#priceRange').slider().off('slideStop');

        $('#priceRange').slider().on('slideStop', function () {
            var newVal = $('#priceRange').data('slider').getValue();

            if (newVal.length > 0) {
                MiniumPrice = newVal[0];
                MaximumPrice = newVal[1];
            }
            $('#hidPageNo').val("1");
            GetProducts();
        });
    }
}

function BindCheckChangeEvent(className, hiddenFieldName, separator) {
    $('body').off('change', className);

    $('body').on('change', className, function () {

        $(hiddenFieldName).val('');

        $('input[type="checkbox"]' + className).each(function () {
            
            if (this.checked == true) {
                $(hiddenFieldName).val($(hiddenFieldName).val() + this.value + separator);
            }

        });

        $('#hidPageNo').val("1");

        GetProducts();

    });
}

function SetDisplayClass() {
    if (isListViewDefault == 'True') {
        $('.prod_list_text_desc').show();
        $('#dvProductData').animate({ opacity: 0 }, 1000, function () {
            $('#dvProductData').removeClass('grid');
            $('#dvProductData').addClass('list');
            $('#dvProductData li').removeClass('col-lg-4 col-md-4 col-sm-6');
            $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
            $('#agrdView').removeClass('hidden');
            $('#alstView').addClass('hidden');
            $('#hdcurview').val("list");
        });
    }
    else if (isGridViewDefault == 'True') {
        $('.prod_list_text_desc').hide();
        $('#dvProductData').animate({ opacity: 0 }, 1000, function () {
            $('#dvProductData').removeClass('list');
            $('#dvProductData').addClass('grid');
            $('#dvProductData li').addClass('col-lg-4 col-md-4 col-sm-6');
            $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
            $('#alstView').removeClass('hidden');
            $('#agrdView').addClass('hidden');
            $('#hdcurview').val("grid");
        });
    }
}

$("body").on("change", "#ddlSorting", function () {
    $('#hidPageNo').val(0);
    $("#hfSelectedSorting").val($(this).find("option:selected").val());
    $('#dvProductData').html('');
    GetProducts();
});

$('body').on('click', '#agrdView', function (e) {
    $('.prod_list_text_desc').hide();
    $('#dvProductData').animate({ opacity: 0 }, 1000, function () {
        $('#dvProductData').removeClass('list');
        $('#dvProductData').addClass('grid');
        $('#dvProductData li').addClass('col-lg-4 col-md-4 col-sm-6');
        $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
        $('#alstView').removeClass('hidden');
        $('#agrdView').addClass('hidden');
        $('#hdcurview').val("grid");
    });
});

$('body').on('click', '#alstView', function (e) {
    $('.prod_list_text_desc').show();
    $('#dvProductData').animate({ opacity: 0 }, 1000, function () {
        $('#dvProductData').removeClass('grid');
        $('#dvProductData').addClass('list');
        $('#dvProductData li').removeClass('col-lg-4 col-md-4 col-sm-6');
        $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
        $('#agrdView').removeClass('hidden');
        $('#alstView').addClass('hidden');
        $('#hdcurview').val("list");
    });
});

$(document).on('click', '#aQuickView', function (e) {
    var id = $(this).attr("productid");
    $('#myModal').find('.modal-body').html('');
    $('#dvLoader').show();
    try {
        $.ajax({
            type: "POST",
            url: host + 'Products/QuickView_ajax.aspx',
            data: {
                productid: id,
                languageid: languageId,
                currencyid: currencyId,
                currencysymbol: currencySymbol
            },
            cache: false,
            async: true
        }).done(function (response) {
            console.log(response);
            $('#dvLoader').hide();
            $('#myModal').find('.modal-dialog').addClass('modal-lg');
            $('#myModal').find('.modal-body').html(response);
            $('#myModal').find('.modal-title').html($('#myModal').find('.modal-body').find('#hidProductName').val());
            $('#myModal').find('.modal-footer').hide();
            $('#myModal').modal('show');
        });
    } catch (e) { }
    finally { $('#dvLoader').hide(); }
});

function SetDisplayClass() {
    if (isBothListAndGridEnabled == 'True') {
        if ($('#alstView').hasClass("hidden")) {
            $('#dvProductData li').removeClass('col-lg-4 col-md-4 col-sm-6');
        }
        else {
            $('#dvProductData li').addClass('col-lg-4 col-md-4 col-sm-6');
        }
    }
}

$(window).scroll(function (event) {
    if (isPaginationEnabled != 'True') {
        SetDisplayClass();
    }
});