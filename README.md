## Changes
- Changed all references of `let` to `var`
- Fixed error on price range slider
- Removed hiding of filter menu when reaching no results are returned.